import { Component } from '@angular/core';
import { DefaultEditor } from './default-editor';
export class InputEditorComponent extends DefaultEditor {
    constructor() {
        super();
    }
}
InputEditorComponent.decorators = [
    { type: Component, args: [{
                selector: 'input-editor',
                styles: [":host input,:host textarea{width:100%;line-height:normal;padding:.375em .75em} /*# sourceMappingURL=editor.component.css.map */ "],
                template: `
    <input [ngClass]="inputClass"
           class="form-control"
           [(ngModel)]="cell.newValue"
           [name]="cell.getId()"
           [placeholder]="cell.getTitle()"
           [disabled]="!cell.isEditable()"
           (click)="onClick.emit($event)"
           (keydown.enter)="onEdited.emit($event)"
           (keydown.esc)="onStopEditing.emit()">
    `,
            },] },
];
/** @nocollapse */
InputEditorComponent.ctorParameters = () => [];
//# sourceMappingURL=input-editor.component.js.map